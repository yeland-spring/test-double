package stub;


import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GradeServiceTest {
    /* 需求描述：
    编写GradeService类的单元测试，单元测试calculateAverageGrades方法
    * */
    @Mock
    GradeSystem gradeSystem;

    @Test
    public void shouldReturn90WhenCalculateStudentAverageGradeAndGradeIs80And90And100() {
        //assertThat(result, is(90.0));
        initMocks(this);
        GradeService gradeService = new GradeService(gradeSystem);
        when(gradeSystem.gradesFor(1)).thenReturn(Arrays.asList(90.0, 80.0, 100.0));
        assertEquals(90.0, gradeService.calculateAverageGrades(1));
    }
}