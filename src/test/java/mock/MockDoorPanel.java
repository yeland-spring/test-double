package mock;

public class MockDoorPanel extends DoorPanel {
    private boolean called = false;

    @Override
    void close() {
        this.called = true;
    }

    public boolean isCalled() {
        return called;
    }
}
